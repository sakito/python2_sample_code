#!/usr/bin/env python
# encoding: utf-8

import tornado.autoreload
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from kc_wrap import KyotoCabinet
import kyotocabinet as kc

tornado.options.define("port", default=5000, help="run on the given port", type=int)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(r"""<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ken_all</title>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript"><!--
  google.load('jquery', '1.4'); // -->
</script>
<script type="text/javascript"><!--
  jQuery( function ($) {
      $('input#zip_code').keydown( function () {
          var $this = $(this);

          setTimeout( function () {
              $.get( '/search', { zip_code: $this.val() }, function (data) {
                  $('div#address').html(data);
              } );
          }, 0 );
      } );

      $('input#zip_code').focus();
  } ); // -->
</script>
</head>
<body>
<form action="/search" method="get">
  <div><input id="zip_code" type="text" maxlength="7"/></div>
  <div id="address"></div>
</form>
</body>
</html>""")


class SearchHandler(tornado.web.RequestHandler):
    def get(self):
        zip_code = self.get_argument("zip_code")
        # ここで kc の検索を実施する
        db = KyotoCabinet()
        db.open("data/KEN_ALL.kct", kc.DB.OREADER)
        # 前方一致検索
        keys = db.match_prefix(zip_code)
        address_lst = []
        for k in keys:
            address_lst.append("%s:%s" % (k[:7], db.get_str(k)))
        db.close()

        for address in address_lst:
            self.write("%s<br />" % address)


def main():
    tornado.options.parse_command_line()
    # デバッグモード
    settings = dict(debug=True)
    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/search", SearchHandler),
    ], **settings)

    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(tornado.options.options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
