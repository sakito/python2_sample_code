#!/usr/bin/env python
# encoding: utf-8

# データをロードする
import csv
import re

from kc_wrap import KyotoCabinet
import kyotocabinet as kc


class DataLoad(object):

    # http://www.post.japanpost.jp/zipcode/dl/readme.html
    csv_file = "data/KEN_ALL.CSV"
    db_file = "data/KEN_ALL.kct"

    def csv_read(self):
        f = csv.reader(file(self.csv_file, "r"))

        db = KyotoCabinet()
        db.open(self.db_file, kc.DB.OWRITER | kc.DB.OCREATE)
        for i, line in enumerate(f):
            # 2: 郵便番号 7桁
            # 6: 都道府県名 漢字
            # 7: 市区町村名 漢字
            # 8: 町域名 漢字
            zip_code = "%s %06d" % (line[2], i)

            # 文字コードの変換
            line[6] = unicode(line[6], 'cp932')
            line[7] = unicode(line[7], 'cp932')
            line[8] = unicode(line[8], 'cp932')

            # http://zipcloud.ibsnet.co.jp/
            # http://d.hatena.ne.jp/bleis-tift/20080531/1212217681
            # 町域名が「以下に掲載がない場合」の場合は、ブランク（空文字）に置換
            # 町域名が「○○市（または町・村）の次に番地がくる場合」の場合は、ブランク（空文字）に置換
            # 町域名が「○○市（または町・村）一円」の場合は、ブランク（空文字）に置換
            if line[8] == u"以下に掲載がない場合" or re.search(u"の次に番地がくる場合", line[8]):
                # 掲載がない場合…などは空白に置換
                line[8] = ""
            else:
                # ○○一円 は空白に置換
                # TODO ただし「一円」が地名である場合は置換しない
                line[8] = re.sub(u"一円", "", line[8])

            # 結合
            address = "%s%s%s" % (line[6], line[7], line[8])

            # 格納
            db.set(zip_code, address)
        # 格納件数を取得
        count = sum(1 for e in db.cursor())
        db.close()
        return count

    def clear(self):
        db = KyotoCabinet()
        db.open(self.db_file, kc.DB.OWRITER | kc.DB.OCREATE)
        db.clear()
        db.close()


def main():
    d = DataLoad()
    d.clear()
    count = d.csv_read()
    print "件数 %d 件" % count

if __name__ == '__main__':
    main()
