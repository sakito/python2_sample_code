#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import unittest

import logging
import logging.config


class LoggingTest(unittest.TestCase):

    def test_root(self):
        """
        初期動作
        """
        # 設定ファイルの読み込み
        logging.config.fileConfig("./log.cfg")

        # logger_root
        log = logging.getLogger()
        log.debug(log.name)
        log.debug("DEBUGログ")
        log.info("INFOログ")
        log.warning("WARNINGログ")
        log.error("ERRORログ")
        log.critical("CRITICALログ")

    def test_file(self):
        """
        ファイル
        """
        # 設定ファイルの読み込み
        logging.config.fileConfig("./log.cfg")

        # logger_file
        log = logging.getLogger("sample_logging")
        log.debug(log.name)
        log.debug("DEBUGログ")
        log.info("INFOログ")
        log.warning("WARNINGログ")
        log.error("ERRORログ")
        log.critical("CRITICALログ")


if __name__ == '__main__':
    unittest.main()
