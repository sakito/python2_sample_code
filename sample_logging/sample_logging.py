#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import logging
import logging.config


def main():
    # 設定ファイルの読み込み
    logging.config.fileConfig("./log.cfg")

    # logger_root
    log = logging.getLogger()
    log.debug(log.name)
    log.debug("DEBUGログ")
    log.info("INFOログ")
    log.warning("WARNINGログ")
    log.error("ERRORログ")
    log.critical("CRITICALログ")

    # logger_apps
    log = logging.getLogger("sample_logging")
    log.debug(log.name)
    log.warning("WARNINGログ")


if __name__ == '__main__':
    main()
