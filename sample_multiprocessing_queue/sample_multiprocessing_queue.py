#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import sys
import cProfile
import multiprocessing


def worker(num, sndq):
    for _ in xrange(num):
        sndq.get()
    sys.exit(1)


def sample():
    # かなり遅いので桁減らしている
    # multiprocessing.Queue は利用したことがないのでまちがっているかも
    #loop_num = 10000000
    loop_num = 1000000
    sndq = multiprocessing.Queue()
    proc = multiprocessing.Process(target=worker, args=(loop_num, sndq))
    #proc.daemon = True
    proc.start()
    for _ in xrange(loop_num):
        msg = "sample message"
        sndq.put(msg)

    for proc in multiprocessing.active_children():
        proc.join()


def main():
    # 実行
    cProfile.run("sample()")

if __name__ == '__main__':
    main()
