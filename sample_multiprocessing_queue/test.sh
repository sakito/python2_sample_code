#/bin/bash

BASEDIR="$(dirname $0)"
LOGFILE="${BASEDIR}/test.log"

for s in sample_multiprocessing_queue.py sample_multiprocessing_zmq.py;do
    echo "== ${s} start ==" >> ${LOGFILE}
    python ${s} >> ${LOGFILE}
    echo "== ${s} end ==" >> ${LOGFILE}
done
