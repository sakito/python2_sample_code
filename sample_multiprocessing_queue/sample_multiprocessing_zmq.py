#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import sys
import cProfile
import multiprocessing
import zmq


def worker(num):
    ctx = zmq.Context()
    rec = ctx.socket(zmq.PULL)
    rec.connect("tcp://127.0.0.1:5555")
    for _ in xrange(num):
        rec.recv()
    sys.exit(1)


def sample():
    loop_num = 10000000
    #loop_num = 1000000
    ctx = zmq.Context()
    sndq = ctx.socket(zmq.PUSH)
    sndq.bind("tcp://127.0.0.1:5555")
    proc = multiprocessing.Process(target=worker, args=(loop_num,))
    #proc.daemon = True
    proc.start()
    for _ in xrange(loop_num):
        msg = "sample message"
        sndq.send(msg)

    for proc in multiprocessing.active_children():
        proc.join()


def main():
    # 実行
    cProfile.run("sample()")

if __name__ == '__main__':
    main()
