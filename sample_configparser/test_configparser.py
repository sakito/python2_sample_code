#!/usr/bin/env python
# encoding: utf-8
# Copyright (C) 2012 sakito <sakito@sakito.com>

import unittest

from ConfigParser import SafeConfigParser, NoSectionError, NoOptionError, ParsingError
import io


class Test(unittest.TestCase):

    def test_simpleconfig(self):
        """
        単純な文字列によるテスト
        """

        sample_config = """\
[sample]
user = username
count = 2
debug = True
"""

        # インスタンスの生成
        config = SafeConfigParser()
        # 文字列を読む
        config.readfp(io.BytesIO(sample_config))
        # とりあえず普通に設定から読む
        self.assertEqual("username", config.get("sample", "user"))
        # get だと 数字でも文字として取れる
        self.assertEqual("2", config.get("sample", "count"))
        # getint だと数字になる
        self.assertEqual(2, config.getint("sample", "count"))
        # boolean として取ることもできる
        self.assertTrue(config.getboolean("sample", "debug"))

        # 存在しない項目を取得した場合 例外が発生
        self.assertRaises(NoOptionError, config.get, "sample", "none")
        self.assertRaises(NoSectionError, config.get, "none", "none")

    def test_allownovalue(self):
        """
        値無しを許容する場合
        """

        sample_config = """\
[sample]
user = username
skip-db
"""
        # 普通にインスタンスを生成
        config = SafeConfigParser()
        # 例外が発生するテスト
        self.assertRaises(ParsingError, config.readfp, io.BytesIO(sample_config))

        # 値無しを許容するには allow_no_value を True にしてインスタンスを生成する
        config = SafeConfigParser(allow_no_value=True)
        config.readfp(io.BytesIO(sample_config))
        self.assertEqual("username", config.get("sample", "user"))
        # None が返ってくる
        self.assertIsNone(config.get("sample", "skip-db"))

    def test_samplefile(self):
        """
        極力大きな設定ファイルのテスト
        """
        config = SafeConfigParser()
        # ファイルを読み込みます
        config.read("sample_file.cfg")
        # 普通に取得できます
        self.assertEqual("samplename", config.get("Sample", "name"))

        self.assertEqual("sample_sub1.cfg", config.get("ListConf", "s1"))
        # 複数項目を取得してみます
        self.assertListEqual([('s1', 'sample_sub1.cfg'),
                         ('s25', 'sample_sub2.cfg'),
                         ('s3', 'sample_sub3.cfg')],
                        config.items("ListConf"))

        # 項目の設定ミスがある物を取得してみます
        self.assertListEqual([('s1', 'sample_sub1.cfg'),
                         ('s25', ''),
                         ('s3', 'sample_sub3.cfg')],
                        config.items("MissListConf"))

        # 置換機能等を確認してみます
        self.assertListEqual([('dir', '/tmp'),
                         ('fulldir', '/tmp/fulldir'),
                         ('longstr', 'long string\nnext line'),
                         ('hoge', 'test')],
                        config.items("SafeConf"))

        # 設定から取得したい値で設定を読んでみる
        fname = config.get("ListConf", "s1")
        subconfig = SafeConfigParser()
        subconfig.read(fname)
        self.assertEqual("subsample", subconfig.get("Sample", "name"))

if __name__ == '__main__':
    unittest.main()
