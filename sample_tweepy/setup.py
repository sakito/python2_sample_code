#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

from setuptools import setup, find_packages

setup(name='sample_tweepy',
      packages=find_packages(),
      install_requires=['tweepy'],
      test_suite='tests',
      )
