#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import unittest
import datetime
import tweepy
from tweepy.error import TweepError


class TestSearch(unittest.TestCase):

    def setUp(self):
        # 現在時刻の取得
        self.now = datetime.datetime.today()

    def test_search(self):
        """
        検索 API
        """
        # API の生成 認証は不要
        api = tweepy.API()

        # 検索クエリ
        query = u"python"
        # 検索
        rets = api.search(q=query,)

        # デフォルトは15件
        self.assertEqual(15, len(rets))

        # 100 件に設定
        rets = api.search(q=query, rpp=100, result_type='recent')
        self.assertEqual(100, len(rets))

        # 1 件に設定
        rets = api.search(q=query, rpp=1, result_type='recent')
        self.assertEqual(1, len(rets))
        # max_id を保持
        max_id = rets.max_id
        print max_id

        #for status in tweepy.Cursor(api.search, q=query, rpp=100).items():
        try:
            # エラーになるまで過去分検索する
            for page in tweepy.Cursor(api.search, q=query, rpp=100, max_id=max_id, result_type='recent').pages():
                print "len = %d, page=%s, max_id=%s" % (len(page), page.page, page.max_id)
        except TweepError, reason:
            print reason
        except TypeError, reason:
            print reason

        # next_page
        # completed_in
        # refresh_url
        # since_id
        # results_per_page
        # query
        # max_id
        # page



if __name__ == '__main__':
    unittest.main()
