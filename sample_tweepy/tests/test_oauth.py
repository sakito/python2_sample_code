#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import unittest
from ConfigParser import SafeConfigParser
import datetime
import tweepy
from tweepy.error import TweepError


class TestOAuth(unittest.TestCase):

    def setUp(self):
        config = SafeConfigParser()
        # 設定ファイルに関してはコミットしない
        config.read("conf/user.sample.cfg.tmp")
        dic = config._sections['user']
        self.user_name = dic.get("user_name")
        self.consumer_key = dic.get("consumer_key")
        self.consumer_secret = dic.get("consumer_secret")
        self.access_token = dic.get("access_token")
        self.access_token_secret = dic.get("access_token_secret")
        # 現在時刻の取得
        self.now = datetime.datetime.today()

    def no_test_oauth(self):
        """
        認証して投稿
        """
        # 認証
        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret, secure=True)
        auth.set_access_token(self.access_token, self.access_token_secret)

        # API の生成
        api = tweepy.API(auth)

        # 名前の確認
        self.assertEqual(self.user_name, api.me().name)

        # 更新
        msg = u"%s YUKI.N>みえてる?" % self.now
        api.update_status(msg)

if __name__ == '__main__':
    unittest.main()
