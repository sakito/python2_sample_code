#!bin/pyming
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import unittest

from ming import datastore as DS

class TestMing(unittest.TestCase):

    def setUp(self):
        ds = DS.DataStore(
            'mongodb://localhost:27017/',
            database='tests')

if __name__ == '__main__':
    unittest.main()
