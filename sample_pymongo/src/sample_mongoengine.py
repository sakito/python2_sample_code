#!bin/pyeng
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import unittest
import datetime
import pymongo
from mongoengine import connect
from mongoengine import Document
from mongoengine import StringField
from mongoengine import EmailField
from mongoengine import DateTimeField
from mongoengine import ValidationError
from mongoengine.connection import get_db, get_connection


class TestMongoEngine(unittest.TestCase):

    def setUp(self):
        # データベースに接続
        connect('tests', username='root', password='password', host='127.0.0.1', port=27017)
        self.conn = get_connection()
        self.db = get_db()

    def tearDown(self):
        # コレクションの削除
        User.drop_collection()

    def test_conect(self):
        """
        データベース接続テスト
        """
        # 接続情報の取得
        self.assertTrue(isinstance(self.conn, pymongo.connection.Connection))

        # DB 情報の取得
        self.assertTrue(isinstance(self.db, pymongo.database.Database))
        self.assertEqual(self.db.name, 'tests')

    def test_create(self):
        """
        create テスト
        """
        user = User(email='jdoe@example.com', first_name='John', last_name='Doe')
        user.save()

        u_obj = User.objects.first()
        u_obj.first_name = "change"
        u_obj.save()

        self.assertEqual(user.first_name, "John")

        # 上書きした場合は reload するまで反映されません
        user.reload()
        self.assertEqual(user.first_name, "change")

    def test_validation(self):
        """
        validation テスト
        """
        user = User()
        self.assertRaises(ValidationError, user.validate)

        user.email = 'valid@example.com'
        user.validate()

        user.first_name = 10
        self.assertRaises(ValidationError, user.validate)

    def test_read(self):
        user = User(email='jdoe@example.com', first_name='John', last_name='Doe')
        user.save()

        collection = self.db[User._get_collection_name()]
        u_obj = collection.find_one({'email': 'jdoe@example.com'})
        self.assertEqual(u_obj['email'], 'jdoe@example.com')
        self.assertEqual(u_obj['first_name'], 'John')
        self.assertEqual(u_obj['last_name'], 'Doe')
        self.assertEqual(u_obj['_id'], user.id)

        u_err = User(email='root@localhost')
        self.assertRaises(ValidationError, u_err.save)
        try:
            u_err.save(validate=False)
        except ValidationError:
            self.fail()


class User(Document):
    email = EmailField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    date_modified = DateTimeField(default=datetime.datetime.now)

if __name__ == '__main__':
    unittest.main()
