#!/bin/bash

# シングルサーバ(1台)で実用的に運用するためのサンプル
# シングルサーバの場合、負荷、容量、安定性などで構成を考える必要がある
# このサンプルではだいたいのシングルサーバで汎用的に利用できる妥当な設定にしてみている

# Replica Set は 必ず作成し、3サーバが必須
# 1台は Arbiter にする
# configsvr は 3台にしている。過去結構死んだりしたので
# mongos も 2から3台あった方が良いのかもしれない。場合による。

# shardsvr：Replica Set 3台 1セット* 3台
#            28201、28202、28203
# configsvr：3台(1台か3台でないと mongos は起動しない)
#           28000、28001、28002
# mongos：ルーティングプロセス 2台以上にするのが普通
#           27017、27018
# -rest を付与して起動すると +1000 port で web UI を見ることができる
# netstat -nap や nmap -p27000-30000 localhost 等でポートの確認をすること
# mongo --port 27017 で接続

MONGO_HOME="${HOME}/bin/mongodb"
MONGO_BIN="${MONGO_HOME}/bin"

MONGO_DATA="${HOME}/var/mongodb"


PATH=${MONGO_BIN}:${PATH}

case $1 in
    setup)
cd ${MONGO_DATA}
mkdir -p c0/data c0/log
mkdir -p c1/data c1/log
mkdir -p c2/data c2/log
mkdir -p s0/data s0/log
mkdir -p s1/data s1/log
mkdir -p d0_0/data d0_1/data d0_2/data d0_0/log d0_1/log d0_2/log

# this is sample
echo -e "Key Files String" > key1
chmod 600 key1
;;
    start)
# mongo config サーバの起動 デフォルトport 27019
mongod \
  --configsvr \
  --rest \
  --port 28000 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/c0/data \
  --logpath ${MONGO_DATA}/c0/log/config.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/c0/log/config.pid \
  --directoryperdb \
  --fork &

mongod \
  --configsvr \
  --rest \
  --port 28001 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/c1/data \
  --logpath ${MONGO_DATA}/c1/log/config.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/c1/log/config.pid \
  --directoryperdb \
  --fork &

mongod \
  --configsvr \
  --rest \
  --port 28002 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/c2/data \
  --logpath ${MONGO_DATA}/c2/log/config.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/c2/log/config.pid \
  --directoryperdb \
  --fork &

# mongno サーバの起動 shardsvr にした場合のデフォルト port 27018
# port は普通は一台のサーバで起動しないのであくまで動作確認用

# mongo サーバの起動 shared0
mongod \
  --replSet shard0 \
  --shardsvr \
  --rest \
  --port 28201 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d0_0/data \
  --logpath ${MONGO_DATA}/d0_0/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d0_0/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard0 \
  --shardsvr \
  --rest \
  --port 28202 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d0_1/data \
  --logpath ${MONGO_DATA}/d0_1/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d0_1/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard0 \
  --shardsvr \
  --rest \
  --port 28203 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d0_2/data \
  --logpath ${MONGO_DATA}/d0_2/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d0_2/log/mongod.pid \
  --directoryperdb \
  --fork &

# ReplicaSetsの設定 1台は arbiterOnly: true を付与する
# mongo --port 28201
# config = {_id:'shard0',members:[{_id:0,host:'localhost:28201'},{_id:1,host:'localhost:28202'},{_id:2,host:'localhost:28203',arbiterOnly: true}]}
# rs.initiate(config);
# rs.status();

;;

    start_admin)
# mongos サーバの起動 デフォルト port 27017
# configdb は 1台か3台でないと起動
mongos \
  --configdb localhost:28000,localhost:28001,localhost:28002 \
  --port 27017 \
  --keyFile ${MONGO_DATA}/key1 \
  --logpath ${MONGO_DATA}/s0/log/mongos.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/s0/log/mongos.pid \
  --fork &

# --chunkSize 1 を付与して起動すると chunk サイズが 1MB になるので動作確認等に利用できる

mongos \
  --configdb localhost:28000,localhost:28001,localhost:28002 \
  --port 27018 \
  --keyFile ${MONGO_DATA}/key1 \
  --logpath ${MONGO_DATA}/s1/log/mongos.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/s1/log/mongos.pid \
  --fork &

# Shardの追加
# mongo --port 27017 admin
# > db.runCommand({addshard:"shard0/localhost:28201"});
# > db.printShardingStatus()

# mongoDBにShardingの設定
# > db.runCommand({enablesharding:"tests"});
# > db.printShardingStatus()

# collectionにShardingの設定
# > db.runCommand({shardcollection:"tests.collectionName",key:{"_id":1}});
# > db.runCommand({shardcollection:"tests.hoge",key:{"_id":1}});
# > db.printShardingStatus()

# パスワードの設定
# > db.addUser('root', 'password')
# > db.system.users.find()
# 削除
# > db.removeUser(username)
# mongo --port 27017 admin -u root -p password
# もしくは
# mongo --port 27017 admin -u root -p
# までうってパスワードを打つことも可能

# Shardingが正常にされているか確認
# mongo --port 28000 config
# > db.shards.find()
# > db.databases.find()
# > db.collections.find()

# mongo --port 28001 config
# > db.shards.find()
# > db.databases.find()
# > db.collections.find()

# mongo --port 28002 config
# > db.shards.find()
# > db.databases.find()
# > db.collections.find()

;;
    stop)
kill -15 `cat ${MONGO_DATA}/s0/log/mongos.pid`
kill -15 `cat ${MONGO_DATA}/s1/log/mongos.pid`
kill -15 `cat ${MONGO_DATA}/c0/log/config.pid`
kill -15 `cat ${MONGO_DATA}/c1/log/config.pid`
kill -15 `cat ${MONGO_DATA}/c2/log/config.pid`
kill -15 `cat ${MONGO_DATA}/d0_0/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d0_1/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d0_2/log/mongod.pid`
;;
    *)
        echo "start or stop"
        exit
esac
