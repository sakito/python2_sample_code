#!/bin/bash

MONGO_HOME="${HOME}/bin/mongodb"
MONGO_BIN="${MONGO_HOME}/bin"

MONGO_DATA="${HOME}/var/mongodb"


PATH=${MONGO_BIN}:${PATH}

# 一台に全サーバを立てているサンプル。普通はこんなことしないが動作確認用

# shardsvr：Replica Set 3台 1セット* 3台
#            28201、28202、28203
#            28211、28212、28213
#            28221、28222、28223
# configsvr：テスト環境では 1台、本番では3台(1台か3台でないと mongos は起動しない)
#           28000、28001、28002
# mongos：ルーティングプロセス 2台以上にするのが普通
#           27017、27018
# -rest を付与して起動すると +1000 port で web UI を見ることができる
# netstat -nap や nmap -p27000-30000 localhost 等でポートの確認をすること
# mongo --port 27017 で接続


case $1 in
    setup)
cd ${MONGO_DATA}
mkdir -p c0/data c0/log
mkdir -p c1/data c1/log
mkdir -p c2/data c2/log
mkdir -p s0/data s0/log
mkdir -p s1/data s1/log
mkdir -p d0_0/data d0_1/data d0_2/data d0_0/log d0_1/log d0_2/log
mkdir -p d1_0/data d1_1/data d1_2/data d1_0/log d1_1/log d1_2/log
mkdir -p d2_0/data d2_1/data d2_2/data d2_0/log d2_1/log d2_2/log

# this is sample
echo -e "Key Files String" > key1
;;
    start)
# mongo config サーバの起動 デフォルトport 27019
mongod \
  --configsvr \
  --rest \
  --port 28000 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/c0/data \
  --logpath ${MONGO_DATA}/c0/log/config.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/c0/log/config.pid \
  --directoryperdb \
  --fork &

mongod \
  --configsvr \
  --rest \
  --keyFile ${MONGO_DATA}/key1 \
  --port 28001 \
  --dbpath ${MONGO_DATA}/c1/data \
  --logpath ${MONGO_DATA}/c1/log/config.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/c1/log/config.pid \
  --directoryperdb \
  --fork &

mongod \
  --configsvr \
  --rest \
  --port 28002 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/c2/data \
  --logpath ${MONGO_DATA}/c2/log/config.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/c2/log/config.pid \
  --directoryperdb \
  --fork &

# mongno サーバの起動 shardsvr にした場合のデフォルト port 27018
# port は普通は一台のサーバで起動しないのであくまで動作確認用

# mongo サーバの起動 shared0
mongod \
  --replSet shard0 \
  --shardsvr \
  --rest \
  --port 28201 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d0_0/data \
  --logpath ${MONGO_DATA}/d0_0/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d0_0/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard0 \
  --shardsvr \
  --rest \
  --port 28202 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d0_1/data \
  --logpath ${MONGO_DATA}/d0_1/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d0_1/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard0 \
  --shardsvr \
  --rest \
  --port 28203 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d0_2/data \
  --logpath ${MONGO_DATA}/d0_2/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d0_2/log/mongod.pid \
  --directoryperdb \
  --fork &

# mongo サーバの起動 shared1
mongod \
  --replSet shard1 \
  --shardsvr \
  --rest \
  --port 28211 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d1_0/data \
  --logpath ${MONGO_DATA}/d1_0/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d1_0/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard1 \
  --shardsvr \
  --rest \
  --port 28212 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d1_1/data \
  --logpath ${MONGO_DATA}/d1_1/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d1_1/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard1 \
  --shardsvr \
  --rest \
  --port 28213 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d1_2/data \
  --logpath ${MONGO_DATA}/d1_2/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d1_2/log/mongod.pid \
  --directoryperdb \
  --fork &

# mongo サーバの起動 shared2
mongod \
  --replSet shard2 \
  --shardsvr \
  --rest \
  --port 28221 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d2_0/data \
  --logpath ${MONGO_DATA}/d2_0/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d2_0/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard2 \
  --shardsvr \
  --rest \
  --port 28222 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d2_1/data \
  --logpath ${MONGO_DATA}/d2_1/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d2_1/log/mongod.pid \
  --directoryperdb \
  --fork &

mongod \
  --replSet shard2 \
  --shardsvr \
  --rest \
  --port 28223 \
  --keyFile ${MONGO_DATA}/key1 \
  --dbpath ${MONGO_DATA}/d2_2/data \
  --logpath ${MONGO_DATA}/d2_2/log/mongod.log \
  --pidfilepath ${MONGO_DATA}/d2_2/log/mongod.pid \
  --directoryperdb \
  --fork &

# ReplicaSetsの設定
# mongo --port 28201
# config = {_id:'shard0',members:[{_id:0,host:'localhost:28201'},{_id:1,host:'localhost:28202'},{_id:2,host:'localhost:28203'}]}
# rs.initiate(config);
# rs.status();

# mongo --port 28211
# config = {_id:'shard1',members:[{_id:0,host:'localhost:28211'},{_id:1,host:'localhost:28212'},{_id:2,host:'localhost:28213'}]}
# rs.initiate(config);
# rs.status();

# mongo --port 28221
# config = {_id:'shard2',members:[{_id:0,host:'localhost:28221'},{_id:1,host:'localhost:28222'},{_id:2,host:'localhost:28223'}]}
# rs.initiate(config);
# rs.status();
;;

    start_admin)
# mongos サーバの起動 デフォルト port 27017
# configdb は 1台か3台でないと起動
mongos \
  --configdb localhost:28000,localhost:28001,localhost:28002 \
  --port 27017 \
  --keyFile ${MONGO_DATA}/key1 \
  --logpath ${MONGO_DATA}/s0/log/mongos.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/s0/log/mongos.pid \
  --fork &
# --chunkSize 1 を付与して起動すると chunk サイズが 1MB になるので動作確認等に利用できる

mongos \
  --configdb localhost:28000,localhost:28001,localhost:28002 \
  --port 27018 \
  --keyFile ${MONGO_DATA}/key1 \
  --logpath ${MONGO_DATA}/s1/log/mongos.log \
  --logappend \
  --pidfilepath ${MONGO_DATA}/s1/log/mongos.pid \
  --fork &

# Shardの追加
# mongo --port 27017 admin
# > db.runCommand({addshard:"shard0/localhost:28201"});
# > db.runCommand({addshard:"shard1/localhost:28211"});
# > db.runCommand({addshard:"shard2/localhost:28221"});
# > db.printShardingStatus()

# mongoDBにShardingの設定
# > db.runCommand({enablesharding:"shardtest"});
# > db.printShardingStatus()

# collectionにShardingの設定
# > db.runCommand({shardcollection:"shardtest.collectionName",key:{"_id":1}});
# > db.runCommand({shardcollection:"shardtest.hoge",key:{"_id":1}});
# > db.printShardingStatus()

# パスワードの設定
# > db.addUser('root', 'password')
# > db.system.users.find()
# 削除
# > db.removeUser(username)

# Shardingが正常にされているか確認
# mongo --port 28000 config
# > db.shards.find()
# > db.databases.find()
# > db.collections.find()

# mongo --port 28001 config
# > db.shards.find()
# > db.databases.find()
# > db.collections.find()

# mongo --port 28002 config
# > db.shards.find()
# > db.databases.find()
# > db.collections.find()

;;
    stop)
kill -15 `cat ${MONGO_DATA}/s0/log/mongos.pid`
kill -15 `cat ${MONGO_DATA}/s1/log/mongos.pid`
kill -15 `cat ${MONGO_DATA}/c0/log/config.pid`
kill -15 `cat ${MONGO_DATA}/c1/log/config.pid`
kill -15 `cat ${MONGO_DATA}/c2/log/config.pid`
kill -15 `cat ${MONGO_DATA}/d0_0/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d0_1/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d0_2/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d1_0/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d1_1/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d1_2/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d2_0/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d2_1/log/mongod.pid`
kill -15 `cat ${MONGO_DATA}/d2_2/log/mongod.pid`
;;
    *)
        echo "start or stop"
        exit
esac
