#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import sys
import multiprocessing
import cProfile


def cpu_work(num):
    """
    引数の回数だけループする関数
    """
    i = 0
    while i < num:
        i += 1


def sample_multiprocess(proc_num):
    loop_num = 10000000

    for _ in xrange(proc_num):
        proc = multiprocessing.Process(target=cpu_work, args=(loop_num / proc_num,))
        proc.deamon = True
        proc.start()

    for proc in multiprocessing.active_children():
        proc.join()


def main():
    # 引数のチェック
    if len(sys.argv) > 1:
        # 実行
        cProfile.run("sample_multiprocess(int(sys.argv[1]))")
    else:
        print "argv"

if __name__ == '__main__':
    main()
