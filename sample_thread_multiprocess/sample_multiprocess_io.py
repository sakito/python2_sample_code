#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import sys
import multiprocessing
import time
import cProfile


def io_work(num):
    """
    i/o の動作の profile がやりやすい time.sleep を利用している
    """
    time.sleep(num)


def sample_multiprocess(proc_num):
    io_num = 10.0

    for _ in xrange(proc_num):
        proc = multiprocessing.Process(target=io_work, args=(io_num,))
        proc.deamon = True
        proc.start()

    for proc in multiprocessing.active_children():
        proc.join()


def main():
    # 引数のチェック
    if len(sys.argv) > 1:
        # 実行
        cProfile.run("sample_multiprocess(int(sys.argv[1]))")
    else:
        print "argv"

if __name__ == '__main__':
    main()
