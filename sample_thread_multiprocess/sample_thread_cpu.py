#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import sys
import threading
import cProfile


def cpu_work(num):
    """
    引数の回数だけループする関数
    """
    i = 0
    while i < num:
        i += 1


def sample_thread(thr_num):
    loop_num = 10000000

    cur_thr = threading.currentThread()
    for _ in xrange(thr_num):
        thr = threading.Thread(target=cpu_work, args=(loop_num / thr_num,))
        thr.deamon = True
        thr.start()

    for thr in threading.enumerate():
        if cur_thr != thr:
            thr.join()


def main():
    # 引数のチェック
    if len(sys.argv) > 1:
        # 実行
        cProfile.run("sample_thread(int(sys.argv[1]))")
    else:
        print "argv"

if __name__ == '__main__':
    main()
