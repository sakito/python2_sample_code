#/bin/bash

BASEDIR="$(dirname $0)"
LOGFILE="${BASEDIR}/test.log"

for s in sample_thread_cpu.py sample_multiprocess_cpu.py sample_thread_io.py sample_multiprocess_io.py;do
    for i in $(seq 1 4); do
        echo "== ${s} ${i} start ==" >> ${LOGFILE}
        python ${s} ${i} >> ${LOGFILE}
        echo "== ${s} ${i} end ==" >> ${LOGFILE}
    done
done
