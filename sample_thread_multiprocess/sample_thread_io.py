#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 sakito <sakito@sakito.com>

import sys
import threading
import time
import cProfile


def io_work(num):
    """
    i/o の動作の profile がやりやすい time.sleep を利用している
    """
    time.sleep(num)


def sample_thread(thr_num):
    io_num = 10.0

    cur_thr = threading.currentThread()
    for _ in xrange(thr_num):
        thr = threading.Thread(target=io_work, args=(io_num,))
        thr.deamon = True
        thr.start()

    for thr in threading.enumerate():
        if cur_thr != thr:
            thr.join()


def main():
    # 引数のチェック
    if len(sys.argv) > 1:
        # 実行
        cProfile.run("sample_thread(int(sys.argv[1]))")
    else:
        print "argv"

if __name__ == '__main__':
    main()
